# Click and Collect France

Ce projet a pour but d’agréger toutes les données disponibles sur les commerçants proposants des services de livraison
ou de _click and collect_ durant cette période de pandémie de COVID19.

## Getting started

```bash
gem install
rake db:migrate
rails s
```

## Scrapers

Les scrapers récupérant les données sont sous la forme de jobs rails. Pour le moment ils sont a executer dans la 
console rails. Par exemple : 

```
rails c
ScrapParisJob.perform_now
```


## Sources


| Nom | Lien | État du scraper | Licence | Description |
|---|---|---|---|---|
| Data Paris | [Dataset](https://parisdata.opendatasoft.com/explore/dataset/coronavirus-commercants-parisiens-livraison-a-domicile/information) | Développé | odbl | |
| CCI (Géo’local) | [Normandie](https://outils.ccimp.com/geolocal-normandie/) | Demande d’autorisation en attente | Non connue | Dispo pour plusieurs départements |


## Todo 

### General

- [ ] Page d’information sur les sources
- [ ] Page à propos

### Front

- [X] Carte basique avec clusters
- [ ] Header avec menu
- [ ] Carte pour chaque établissement (nom, addresse, téléphone, lien vers la source, etc)

### DevOps

- [ ] CI (lint, tests)
- [ ] CD

