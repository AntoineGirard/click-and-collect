require 'net/http'
require 'json'
require 'pp'

class ScrapParisJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    response = Net::HTTP.get_response('parisdata.opendatasoft.com', '/api/records/1.0/search/?dataset=coronavirus-commercants-parisiens-livraison-a-domicile&q=&rows=10000&facet=code_postal&facet=type_de_commerce&facet=services')
    if response.code == '200'
      parsed = JSON.parse(response.body)

      parsed['records'].each do |shop|
        s = Shop.where(sourceid: 'parisdata_' + shop['recordid']).first_or_initialize
        pp shop['geometry'].to_s
        s.assign_attributes(
          datasetid: shop['datasetid'],
          sourceid: 'parisdata_' + shop['recordid'],
          name: shop['fields']['nom_du_commerce'],
          description: shop['fields']['description'],
          address: shop['fields']['adresse'],
          postcode: shop['fields']['code_postal'],
          geometry: shop['geometry'].to_json,
          shop_type: shop['fields']['type_de_commerce'],
          delivery: shop['fields']['services'] && (shop['fields']['services'].include? 'Livraisons à domicile'),
          collect: shop['fields']['services'] && (shop['fields']['services'].include? 'Retrait de commandes en magasin'),
          mail: shop['fields']['mail'],
          phone: shop['fields']['telephone']
        )
        s.save
      end
    end
  end
end
