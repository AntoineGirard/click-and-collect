const mymap = L.map('mapid').setView([46, 1], 7);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZ2lyYXJkYW50byIsImEiOiJja2hlejgyNTUwNm1yMnRxcDY1dmpkcDRlIn0.SesL2crcUPlQQF38WYtBGQ'
}).addTo(mymap);

var markers = L.markerClusterGroup();

axios.get('/shop/list')
    .then(function (response) {
        var geoJsonLayer = L.geoJSON(response.data, {
            onEachFeature: function (feature, layer) {
                layer.bindPopup("<b>" + feature.properties.name + "</b> <em>" + feature.properties.type + "</em> " + feature.properties.address);
            }
        });
        markers.addLayer(geoJsonLayer)

        mymap.addLayer(markers)
        mymap.fitBounds(markers.getBounds())
    })
    .catch(function (error) {
        console.log(error)
    });
