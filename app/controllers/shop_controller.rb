require 'json'

class ShopController < ApplicationController
  def index; end

  def list
    result = []
    s = Shop.all
    s.each do |shop|
      result.push({
                    type: 'Feature',
                    properties: {
                      name: shop.name,
                      description: shop.description,
                      type: shop.shop_type,
                      phone: shop.phone,
                      address: shop.address,
                      postcode: shop.postcode,
                      delivery: shop.delivery,
                      collect: shop.collect
                    },
                    geometry: JSON.parse(shop.geometry)
                  })
    end
    render json: result
  end
end
