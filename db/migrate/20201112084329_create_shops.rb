class CreateShops < ActiveRecord::Migration[6.0]
  def change
    create_table :shops do |t|
      t.string :datasetid, null: false
      t.string :sourceid, null: false

      t.string :name, null: false
      t.text :description
      t.string :address, null: false
      t.string :postcode, null: false
      t.string :lat, null: false
      t.string :lon, null: false
      t.string :type

      t.boolean :delivery
      t.boolean :collect

      t.string :mail
      t.string :phone

      t.timestamps
    end
  end
end
