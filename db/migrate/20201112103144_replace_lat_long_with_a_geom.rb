class ReplaceLatLongWithAGeom < ActiveRecord::Migration[6.0]
  def change
    remove_column :shops, :lat, :string, null: false
    remove_column :shops, :lon, :string, null: false
    add_column :shops, :geometry, :text, null: false
  end
end
