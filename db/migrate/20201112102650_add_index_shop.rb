class AddIndexShop < ActiveRecord::Migration[6.0]
  def change
    add_index :shops, :sourceid, :unique => true
  end
end
