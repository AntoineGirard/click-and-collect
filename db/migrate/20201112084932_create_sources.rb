class CreateSources < ActiveRecord::Migration[6.0]
  def change
    create_table :sources do |t|
      t.string :name
      t.string :license
      t.string :url
      t.datetime :last_scrap

      t.timestamps
    end
  end
end
