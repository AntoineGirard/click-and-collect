class RenameShopTypeColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :shops, :type, :shop_type
  end
end
