# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_12_120030) do

  create_table "shops", force: :cascade do |t|
    t.string "datasetid", null: false
    t.string "sourceid", null: false
    t.string "name", null: false
    t.text "description"
    t.string "address", null: false
    t.string "postcode", null: false
    t.string "shop_type"
    t.boolean "delivery"
    t.boolean "collect"
    t.string "mail"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "geometry", null: false
    t.index ["sourceid"], name: "index_shops_on_sourceid", unique: true
  end

  create_table "sources", force: :cascade do |t|
    t.string "name"
    t.string "license"
    t.string "url"
    t.datetime "last_scrap"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
